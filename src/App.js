import './App.css';
import { BrowserRouter as Router, Switch,Route, Link } from "react-router-dom";
import Header from './containers/Header';
import PoductComponent from './containers/PoductComponent';
import ProductDetail from './containers/ProductDetail';
import ProductListing from './containers/ProductListing';


 
function App() {
  return (
    <div>
      <Router>
    <Header/>
    <Switch>
    <Route path="/" exact component={ProductListing}/>
    <Route path="/product/:poductId" exact component={ProductDetail}/>
    <Route>404 not found page!</Route>
    </Switch>
    {/* <PoductComponent/>
    <ProductDetail/>
    <ProductListing/> */}
    </Router>
    </div>
  );
}

export default App;
