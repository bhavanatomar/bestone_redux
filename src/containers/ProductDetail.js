import React, { useEffect, useCallback, useMemo } from "react";
import axios from "axios";
import { useParams } from "react-router-dom";

import { useDispatch, useSelector } from "react-redux";
// import { useParams } from "react-router";
 import { setProducts } from "../redux/actions/productActions";
// import ProductComponent from "./ProductComponent";

const ProductDetail = () => {
  const products = useSelector((state) => state.allProducts.products);
  const {poductId}=useParams();
  const dispatch = useDispatch();
  const fetchProducts = async () => {
    const response = await axios
      .get( `https://crud-prod-back.herokuapp.com/api/${poductId}`)
      .catch((err) => {
        console.log("Err: ", err);
      });
    dispatch(setProducts(response.data));
  };

  useEffect(() => {
    fetchProducts();
  }, []);

  console.log("Products :", products);
  return (
    <div className="ui grid container">
      {/* <ProductComponent /> */}
    </div>
  );
};

export default ProductDetail;