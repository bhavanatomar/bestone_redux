import React, { useEffect, useCallback, useMemo } from "react";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { setProducts } from "../redux/actions/productActions";
 import ProductComponent from "./PoductComponent";

 const ProductListing = () => {
  //  const products = useSelector((state) => state.allProducts.products);
   const products = useSelector((state) => state);
console.log("products===",products)
  const dispatch = useDispatch();
  const fetchProducts = async () => {
    const response = await axios
      .get(" https://crud-prod-back.herokuapp.com/api")
      .catch((err) => {
        console.log("Err: ", err);
      });
     dispatch(setProducts(response.data));
  };

  useEffect(() => {
    fetchProducts();
  }, []);

  return (
    <div className="ui grid container">
    <ProductComponent/>
    </div>
  );
 };

export default ProductListing;